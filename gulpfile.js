'use strict';

// Node Modules
var del = require('del');
var runSequence = require('run-sequence');
var yargs = require('yargs');

// Gulp Modules
var gulp = require('gulp');
var $ = require('gulp-load-plugins')({ lazy: true });

var argv = yargs.argv;
var isRelease = argv.release;

// -----------------------------------------------------------------------------
// Project Initialization Tasks
// -----------------------------------------------------------------------------

// gulp.task('init', function () {
// });

// -----------------------------------------------------------------------------
// Build Tasks
// -----------------------------------------------------------------------------

gulp.task('build', function () {
  runSequence(
    'build:scss',
    'copy:src',
    'download:drupal-upstream'
  );
});

gulp.task('build:scss', function () {
  return gulp.src('src/themes/drupal_jenkins_test/sass/main.scss')
    .pipe($.if(!isRelease, $.sourcemaps.init({ loadMaps: true })))
    .pipe($.sass({
      outputStyle: isRelease ? 'compressed' : 'nested',
      sourceComments: isRelease ? false : 'map'
    })
    .on('error', $.sass.logError))
    .pipe($.if(!isRelease, $.sourcemaps.write()))
    .pipe(gulp.dest('src/themes/drupal_jenkins_test/css'));
});

// -----------------------------------------------------------------------------
// Clean Tasks
// -----------------------------------------------------------------------------

gulp.task('clean', function (cb) {
  del(['docroot/**', 'build/**', 'dist/**'], cb);
});

// -----------------------------------------------------------------------------
// Copy Tasks
// -----------------------------------------------------------------------------

gulp.task('copy:src', function () {
  runSequence([
    'copy:modules',
    'copy:profiles',
    'copy:sites',
    'copy:themes',
    'copy:templates'
  ]);
});

gulp.task('copy:modules', function () {
  return gulp.src('src/modules/**')
    .pipe(gulp.dest('docroot/modules/custom/'));
});

gulp.task('copy:profiles', function () {
  return gulp.src('src/profiles/**')
    .pipe(gulp.dest('docroot/profiles/custom/'));
});

gulp.task('copy:sites', function () {
  return gulp.src('src/sites/**')
    .pipe(gulp.dest('docroot/sites/'));
});

gulp.task('copy:themes', function () {
  return gulp.src('src/themes/**')
    .pipe(gulp.dest('docroot/themes/custom/'));
});

gulp.task('copy:templates', function () {
  return gulp.src(
    [
      'templates/drupal/.htaccess',
      'templates/drupal/autoload.php',
      'templates/drupal/robots.txt'
    ])
    .pipe(gulp.dest('docroot/'));
});

// -----------------------------------------------------------------------------
// Download Tasks
// -----------------------------------------------------------------------------

gulp.task('download:drupal-upstream', function () {
  $.remoteSrc(['index.php', 'update.php', 'web.config'], {
    base: 'https://raw.githubusercontent.com/drupal/drupal/8.0.6/'
  })
  .pipe(gulp.dest('docroot/'));
});

// -----------------------------------------------------------------------------
// Package Tasks
// -----------------------------------------------------------------------------

gulp.task('package', function () {
  $.util.log('Packaging build version ' + argv.version + ' to dist');
  return gulp.src(['*docroot/**', '*vendor/**', '*config/**', '*drush/**', '!**/.git', '!**/.git/**'], { dot: true })
    .pipe($.tar(argv.version + '.tar'))
    .pipe($.gzip())
    .pipe(gulp.dest('dist/'));
});
