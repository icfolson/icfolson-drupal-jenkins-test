<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}

// Site icfolsonsites, local DrupalVM (vagrant) environment.
// Note: The remote-host can either be your local IP or a domain if you have
// modified your hosts file.
$aliases['loc'] = array(
  'uri' => 'jenkinstesting.loc',
  'root' => '/home/vagrant/project/docroot'
);

// Site jenkinstesting, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/jenkinstesting.dev/docroot',
  'ac-site' => 'jenkinstesting',
  'ac-env' => 'dev',
  'ac-realm' => 'devcloud',
  'uri' => 'jenkinstestingfet7hv3jyd.devcloud.acquia-sites.com',
  'remote-host' => 'free-5155.devcloud.hosting.acquia.com',
  'remote-user' => 'jenkinstesting.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);

// Site jenkinstesting, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/jenkinstesting.test/docroot',
  'ac-site' => 'jenkinstesting',
  'ac-env' => 'test',
  'ac-realm' => 'devcloud',
  'uri' => 'jenkinstestingdyavbp2dtb.devcloud.acquia-sites.com',
  'remote-host' => 'free-5155.devcloud.hosting.acquia.com',
  'remote-user' => 'jenkinstesting.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
