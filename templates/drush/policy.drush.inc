<?php
/**
 * @file
 * Drush policy file.
 */

/**
 * Implements drush_hook_COMMAND_validate().
 *
 * Prevent @prod as a destination of sql-sync.
 *
 * Note: This file has to be local to the machine or VM that initiates the
 * sql-sync command.
 */
function drush_policy_sql_sync_validate($source = NULL, $destination = NULL) {
  if (strstr($destination, '.prod')) {
    return drush_set_error(dt('Per policy.drush.inc, you may never overwrite the production database.'));
  }
}

/**
 * Implements drush_hook_COMMAND_validate().
 *
 * Prevent @prod as a destination of rsync
 *
 * Note: This file has to be local to the machine or VM that initiates the
 * sql-sync command.
 */
function drush_policy_rsync_validate($source = NULL, $destination = NULL) {
  if (strstr($destination, '.prod')) {
    return drush_set_error(dt('Per policy.drush.inc, you may never overwrite the production files.'));
  }
}
