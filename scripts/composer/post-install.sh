#!/bin/bash

_site_install() {
  echo "Installing site."
  _drupal_scaffold
  drush @jenkinstesting.loc sql-drop -y
  drush @jenkinstesting.loc sql-cli < ./drush/backups/db/base_install.sql
  drush @jenkinstesting.loc config-import -y
  gulp build:scss
}

_drupal_scaffold() {
  echo "Copying files from upstream Drupal repository"
  if [ ! -f docroot/index.php ]; then
    wget https://raw.githubusercontent.com/drupal/drupal/8.0.6/index.php -P ./docroot
  fi
  if [ ! -f docroot/update.php ]; then
    wget https://raw.githubusercontent.com/drupal/drupal/8.0.6/update.php -P ./docroot
  fi
  if [ ! -f docroot/web.config ]; then
    wget https://raw.githubusercontent.com/drupal/drupal/8.0.6/web.config -P ./docroot
  fi
}

# Prepare a local storage location for the files folders.
if [ ! -d storage ]; then
  echo "Making storage directory."
  mkdir -m777 storage
fi
if [ ! -d storage/files ]; then
  echo "Making storage/files directory."
  mkdir -m777 storage/files
fi
if [ ! -d storage/files_private ]; then
  echo "Making storage/files_private directory."
  mkdir -m777 storage/files_private
fi

# Prepare the settings and services file for installation
if [ ! -f docroot/sites/default/settings.php ]; then
  echo "Creating settings.php symlink."
  mkdir -m755 docroot/sites
  mkdir -m755 docroot/sites/default
  # Use a relative path symlink to allow for vagrant shared folder access.
  ln -s ../../../src/sites/default/settings.php docroot/sites/default/settings.php
fi
if [ ! -f docroot/sites/default/services.yml ]; then
  echo "Creating services.yml symlink."
  # Use a relative path symlink to allow for vagrant shared folder access.
  ln -s ../../../src/sites/default/services.yml docroot/sites/default/services.yml
fi
if [ ! -d docroot/sites/default/files ]; then
  echo "Creating docroot/sites/default/files symlink."
  ln -s ../../../storage/files docroot/sites/default/files
fi

# The settings.php looks for settings.local.php based on the physical location
# of settings.php. So we symlink from the physical location.
if [ ! -f src/sites/default/settings.local.php ]; then
  if [ ! -f storage/settings.local.php ]; then
    echo "Copying example.settings.local.php to storage/settings.local.php."
    cp templates/drupal/settings.local.php storage/settings.local.php
  fi

  # Use a relative path symlink to allow for vagrant shared folder access.
  echo "Creating settings.local.php symlink."
  ln -s ../../../storage/settings.local.php src/sites/default/settings.local.php
fi

# Create symlinks for our custom modules, themes, and profiles.
if [ ! -d docroot/modules/custom ]; then
  echo "Creating docroot/modules/custom symlink."
  # Use a relative path symlink to allow for vagrant shared folder access.
  ln -s ../../src/modules docroot/modules/custom
fi

if [ ! -d docroot/themes/custom ]; then
  echo "Creating docroot/themes/custom symlink."
  mkdir -m755 docroot/themes
  # Use a relative path symlink to allow for vagrant shared folder access.
  ln -s ../../src/themes docroot/themes/custom
fi

if [ ! -d docroot/profiles/custom ]; then
  echo "Creating docroot/profiles/custom symlink."
  mkdir -m755 docroot/profiles
  # Use a relative path symlink to allow for vagrant shared folder access.
  ln -s ../../src/profiles docroot/profiles/custom
fi

# Copy drush files.
if [ ! -d ~/.drush ]; then
  echo "Unable to locate ~/.drush folder to copy drush configuration"
else
  for f in templates/drush/*
  do
    filename=$(basename "$f")
    if [ ! -f ~/.drush/$filename ]
    then
      echo "Copying $f to ~/.drush folder."
      cp $f ~/.drush
    else
      echo "$filename already exists. Not copying."
    fi
  done
fi

# Move necessary template files in to docroot
if [ ! -f docroot/.htaccess ]; then
  echo "Copying templates/drupal/.htaccess to docroot/.htaccess."
  cp templates/drupal/.htaccess docroot/.htaccess
fi
if [ ! -f docroot/autoload.php ]; then
  echo "Copying templates/drupal/autoload.php to docroot/autoload.php."
  cp templates/drupal/autoload.php docroot/autoload.php
fi
if [ ! -f docroot/robots.txt ]; then
  echo "Copying templates/drupal/robots.txt to docroot/robots.txt."
  cp templates/drupal/robots.txt docroot/robots.txt
fi

# Install the Drupal site using the drush alias.
if drush sa | grep -q '@jenkinstesting.loc'; then
  _site_install
elif [ -f ~/.drush/jenkinstesting.aliases.drushrc.php ]; then
  _site_install
else
  echo 'Unable to locate drush site alias @jenkinstesting.loc.'
fi
